from flask import Flask, request, jsonify

from Constants import GITLAB_SECRET_TOKEN
from handlers.labelUpdateHandler import handle_label_update
from handlers.mrHandler import handle_merge_request_creation

app = Flask(__name__)

@app.route('/webhook', methods=['POST'])
def gitlab_webhook():
    if request.headers.get('X-Gitlab-Token') != GITLAB_SECRET_TOKEN:
        return jsonify({"message": "Invalid secret token"}), 403

    event = request.json
    object_kind = event.get('object_kind')
    action = event.get('object_attributes').get('action')
    
    if action == 'update':
        return handle_label_update(event)
    elif object_kind == 'merge_request':
        return handle_merge_request_creation(event)

    return jsonify({"message": "No action taken"}), 404

if __name__ == '__main__':
    app.run(debug=True, port=5000)
