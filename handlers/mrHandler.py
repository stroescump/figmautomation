import requests
import re

from handlers.figmaHelper import post_comment_to_figma

def handle_merge_request_creation(event):
    mr = event.get('object_attributes', {})
    action = mr.get('action')

    if action in ['open', 'reopen']:
        labels = event.get('labels', [])
        if any(label['title'] == 'Figma' for label in labels):
            body = mr.get('description', '')
            layer_match = re.search(r'Layer:\s*(\S+)', body)

            if layer_match:
                layer_name = layer_match.group(1)
                try:
                    mr_details_url = mr.get('url')
                    comment = f"Link for GitLab MR: {mr_details_url}"
                    post_comment_to_figma(layer_name, comment)
                    return {"message": "Comment posted to Figma"}
                except requests.HTTPError as e:
                    return {"error": f"Failed to post comment to Figma: {e}"}, e.response.status_code
                except Exception as e:
                    return {"error": str(e)}, 500
        return {"message": "Did not find any Figma label"}, 404
    return {"message": "No action taken for MR creation or re-opening"}