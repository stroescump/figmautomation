import requests

from Constants import FIGMA_FILE_KEY, FIGMA_TOKEN

def post_comment_to_figma(layer_name, comment):
    file_data = get_figma_file_data()
    layer_id = find_layer_by_name(file_data, layer_name)
    
    if not layer_id:
        raise Exception(f"Layer with name '{layer_name}' not found in Figma file.")

    url = f"https://api.figma.com/v1/files/{FIGMA_FILE_KEY}/comments"
    headers = {
        "X-Figma-Token": FIGMA_TOKEN,
        "Content-Type": "application/json"
    }
    data = {
        "message": comment,
        "client_meta": {
            "node_id": layer_id,
            "node_offset": {
                "x": 20,  # Adjust x coordinate as needed
                "y": 20   # Adjust y coordinate as needed
            }
        }
    }
    response = requests.post(url, json=data, headers=headers)
    return response

def get_figma_file_data():
    url = f'https://api.figma.com/v1/files/{FIGMA_FILE_KEY}'
    headers = {
        'X-Figma-Token': FIGMA_TOKEN
    }
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        raise Exception(f"Failed to fetch Figma file data: {response.status_code}, {response.text}")

def find_layer_by_name(file_data, layer_name):
    def search_layers(layers):
        for layer in layers:
            if layer['name'] == layer_name:
                return layer['id']
            if 'children' in layer:
                result = search_layers(layer['children'])
                if result:
                    return result
        return None

    document = file_data.get('document', {})
    return search_layers(document.get('children', []))