import requests
import re

from handlers.figmaHelper import post_comment_to_figma

def handle_label_update(event):
    mr = event.get('object_attributes', {})
    
    labels = event.get('labels', [])
    figma_label_added = any(label['title'] == 'Figma' for label in labels)

    if figma_label_added:
            try:
                mr_details_url = mr.get('url')
                body = mr.get('description', '')
                layer_match = re.search(r'Layer:\s*(\S+)', body)
                if layer_match:
                    layer_name = layer_match.group(1)
                    comment = f"Link for GitLab MR: {mr_details_url}"
                    try:
                        post_comment_to_figma(layer_name, comment)
                        return {"message": "Comment posted to Figma"}
                    except requests.HTTPError as e:
                        return {"error": f"Failed to post comment to Figma: {e}"}, e.response.status_code
                else: return {"error": "No layer was found in your MR's body"}, 404
            except Exception as e:
                return {"error": str(e)}, 500
    else: return {"error": "No Figma labels found"}, 200