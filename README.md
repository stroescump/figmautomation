# Figma Automation
<b>A webhook dedicatd to GitLab & Figma.<b> 

Posting a comment on a given Figma screen so that relevant stakeholders have easy access to your MR.<br><br>
It triggers a comment on MR creation/re-opening and label insertion.


## Getting started
- Clone the repo.
- Install `ngrok` using `brew install ngrok/ngrok/ngrok`
- Make sure you have installed Python and you have set environment variables correctly.
- Install dependencies required for the server to run locally using `pip install Flask requests`
- Using terminal run `python3 app.py` or `flask run` from the root of the folder.
- Create an MR, use label `Figma` and in the body of the MR make sure to include the name of the screen in Figma where you want the comment to be posted, e.g<br>`Layer: mobile_proprietary_name`
- Open Figma and watch the magic happen ([LINK](https://www.figma.com/design/WDwgMLi3jV2hHMkjWxYKe2/Untitled?node-id=1-3&t=tFGZ6po6WLka8DIW-1))







